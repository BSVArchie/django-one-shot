from django.urls import path
from todos.views import (get_list, lst_tasks, create_list,
                         edit_list, delete_list, create_task,
                         update_item,)

urlpatterns = [
    path("", get_list, name="todo_list_list"),
    path("<int:id>/", lst_tasks, name="todo_list_detail"),
    path("create/", create_list, name="todo_list_create"),
    path("<int:id>/edit/", edit_list, name="todo_list_update"),
    path("<int:id>/delete/", delete_list, name="todo_list_delete"),
    path("items/create/", create_task, name="todo_item_create"),
    path("items/<int:id>/edit/", update_item, name="todo_item_update"),
]
