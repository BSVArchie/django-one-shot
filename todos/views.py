from django.shortcuts import render, get_object_or_404, redirect
from todos.models import TodoList, TodoItem
from todos.forms import TodoForm, ItemForm

# Create your views here.

def get_list(request):
    list_thing = TodoList.objects.all()
    context = {
        "list_object": list_thing
    }
    return render(request, "todos/lst.html", context)

def lst_tasks(request, id):
    tasks = get_object_or_404(TodoList, id=id)
    context = {
        "task_object": tasks,
    }
    return render(request, "todos/detail.html", context)

def create_list(request):
    if request.method == "POST":
        form = TodoForm(request.POST)
        if form.is_valid():
            list = form.save()
        return redirect("todo_list_detail", id=list.id)
    else:
        form = TodoForm
    context = {
        "create_form_key": form
    }
    return render(request, "todos/create.html", context)

def edit_list(request, id):
    lst = get_object_or_404(TodoList, id=id)
    if request.method == "POST":
        form = TodoForm(request.POST, instance=lst)
        if form.is_valid():
            list = form.save()
        return redirect("todo_list_detail", id=list.id)
    else:
        form = TodoForm
        context = {
            "list_object": lst,
            "edit_form_key": form
        }
    return render(request, "todos/edit.html", context)

def delete_list(request, id):
    lst = TodoList.objects.get(id=id)
    if request.method == "POST":
        lst.delete()
        return redirect("todo_list_list")

    return render(request, "todos/delete.html")

def create_task(request):
    if request.method == "POST":
        form = ItemForm(request.POST)
        if form.is_valid():
            item = form.save()
            return redirect("todo_list_detail", id=item.list.id)
    else:
        form = ItemForm
        context = {
            "TodoItem_key": form
        }
    return render(request, "todos/items_create.html", context)

def update_item(request, id):
    task = get_object_or_404(TodoItem, id=id)
    #task = TodoItem.objects.get(id=id)
    if request.method == "POST":
        form = ItemForm(request.POST, instance=task)
        if form.is_valid():
            item = form.save()
            return redirect("todo_list_detail", id=item.list.id)
    else:
        form = ItemForm(instance=task)
        context = {
            "update_item_key": form
        }
    return render(request, "todos/items_update.html", context)
